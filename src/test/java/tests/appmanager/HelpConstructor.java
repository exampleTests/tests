package tests.appmanager;
import org.openqa.selenium.By;


import static com.codeborne.selenide.Selenide.$;

public class HelpConstructor {

    protected void click(By locator) {
        $(locator).click();
    }
    protected void type(By locator, String text) {
        $(locator).clear();
        $(locator).sendKeys("");
        $(locator).sendKeys(text);
    }
    protected void type(By locator, int text) {
        $(locator).clear();
        $(locator).sendKeys("");
        $(locator).sendKeys(String.valueOf(text));
    }

}
