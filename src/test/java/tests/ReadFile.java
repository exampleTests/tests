package tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class ReadFile {


    public static void main(String[] arg) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("src/test/resources/file.csv")));
        String[] numbers = contents.split(",");
        int[] array = Arrays.stream(numbers).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }

    }

}
