package tests.appmanager;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class NavigationHelper extends HelpConstructor{

    public void mobileMenu() {
        click(By.xpath("//*[ text()='Мобильные телефоны']"));
    }

    public void electronic() {
        $(By.xpath("//*[@class='n-w-tabs__horizontal-tabs n-adaptive-layout']")).waitUntil(exist,4000);
        $(By.xpath("//*[@class='n-w-tab__control-caption' and text()='Электроника']")).waitUntil(visible, 4000);
        click(By.xpath("//*[@class='n-w-tab__control-caption' and text()='Электроника']"));
    }

    public void market() {
        click(By.xpath("//*[@class='home-link home-link_blue_yes home-tabs__link home-tabs__search' and text()='Маркет']"));
    }

    public void headphonesMenu() {
        click(By.xpath("//*[contains(text(),'Наушники')]"));
    }
}
