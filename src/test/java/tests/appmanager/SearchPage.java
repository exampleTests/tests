package tests.appmanager;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import tests.model.ProductData;

import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.testng.AssertJUnit.assertTrue;

public class SearchPage extends HelpConstructor {

    public void inputFilters(ProductData product) {

        clickCheckbox(product);
        $(By.tagName("h1")).waitUntil(text(product.getMark()),4000);
        inputPriceFrom(product);
        inputPriceTo(product);
    }
    public void clickCheckbox(ProductData product) {
        click(By.xpath("//fieldset[@class='_3M70uokkTS']//*[@class ='NVoaOvqe58' and text()='"+ product.getMark() +"']"));
    }
    public void inputPriceFrom(ProductData product){
        type(By.id("glpricefrom"), product.getPriceFrom());
    }
    public void inputPriceTo(ProductData product){
        type(By.id("glpriceto"), product.getPriceTo());

    }
    public void assertFiltersTrue(ProductData product) {
        $(By.xpath("//*[@class='_2E5mtWI4YB _2b6OiXTTPs _3m5L8ZAmG5']")).waitUntil(visible, 10000);
        int elemNumbers = $$(By.xpath("//div[@class='price']")).size();
        Configuration.timeout = 5000;
        for (int i = 2; i < elemNumbers; i++) {
            String text = $(By.xpath("(//div[@class='price'])[" + i + "]")).getText();
            String fixPrice = text.replaceAll("\\D+", "");
            assertTrue(product.getPriceFrom() <= Integer.parseInt(fixPrice));


        }
    }

    public void choiceProduct(int index) {
        $(By.xpath("(//*[@class = 'image'])["+index+"]")).click();
    }

    public String getProductName(int index) {
        Configuration.timeout = 4000;
      return $(By.xpath("(//*[@class = 'n-snippet-cell2__title'])["+index+"]")).getText();

    }
}
