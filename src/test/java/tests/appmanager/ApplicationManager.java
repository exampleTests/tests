package tests.appmanager;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ApplicationManager {
    private WebDriver wd;
    private Properties properties;
    private SearchPage searchPage;
    private NavigationHelper navigationHelper;
    private ProductPage productPage;

    public void init() throws IOException {

        String target = System.getProperty("target", "local");
        properties = new Properties();
        searchPage = new SearchPage();
        navigationHelper = new NavigationHelper();
        productPage = new ProductPage();
        properties.load(new FileReader(new File("src/test/resources/" + target + ".properties")));
        String browser = properties.getProperty("browser");

        if (browser.equals("chrome")) {
            wd = new ChromeDriver();
            WebDriverRunner.setWebDriver(wd);
            wd.manage().window().maximize();

        } else if (browser.equals("firefox")) {
            wd = new FirefoxDriver();
            WebDriverRunner.setWebDriver(wd);
            wd.manage().window().maximize();

        }

        wd.get("https://yandex.ru");

    }


    public Properties properties(){return properties;}
    public SearchPage searchPage(){return searchPage;}
    public NavigationHelper goTo(){return navigationHelper;}
    public ProductPage product(){return productPage;}


    public void close() {
        Selenide.close();
    }
}


