package tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class AlfaTest {

    private WebDriver wd;
    private String browser = "chrome";
    private String search = "google";

    @BeforeSuite
    public void init(){
        if (browser.equals("chrome")) {
            wd = new ChromeDriver();
            WebDriverRunner.setWebDriver(wd);
            wd.manage().window().maximize();

        } else if (browser.equals("firefox")) {
            wd = new FirefoxDriver();
            WebDriverRunner.setWebDriver(wd);
            wd.manage().window().maximize();

        }
    }

    @BeforeMethod
    public void setUp(){
        wd.get("https://"+search+".ru");
    }
    @Test
    public void alfaTest() {
        String date = getDate();
        inputRequestInSearch();
        openAlfaBankPage();
        switchToSecondTab();
        openAlfaBankJobPage();
        List<String> list = getTextFromPage();
        inputTextToFile(date, list,browser,search);

    }

    @AfterMethod
        public void close(){
        Selenide.close();
    }






    private void inputTextToFile(String date, List<String> list, String browser,String search) {
        try (FileOutputStream fos = new FileOutputStream("src/test/resources/files/"+date+"_"+browser+"_"+search+".csv")) {
            for (String aList : list) {
                byte[] buffer = aList.getBytes();

                fos.write(buffer, 0, buffer.length);
            }
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    private List<String> getTextFromPage() {
        $(By.xpath("//*[@href='/home']")).click();
        List<String> list = new ArrayList<>();
        list.add($(By.xpath("//*[@class='top-32']//*[@class='aligncenter font-white font-18 font-sm-16']")).getText());
        list.add($(By.xpath("(//*[@class='top-32']//*[@class='aligncenter font-white font-18 font-sm-16 top-12'])[1]")).getText());
        list.add($(By.xpath("(//*[@class='top-32']//*[@class='aligncenter font-white font-18 font-sm-16 top-12'])[2]")).getText());
        return list;
    }

    private void openAlfaBankJobPage() {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        js.executeScript("window.scrollBy(0,2000)");
        $(By.xpath("//*[@href='http://job.alfabank.ru/']")).click();
        $(By.tagName("h2")).shouldHave(text("Работа в Альфа-Банке"));
    }

    private void switchToSecondTab() {
        WebDriver driver = WebDriverRunner.getWebDriver();
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        driver.close();
        driver.switchTo().window(tabs.get(1));
    }

    private void openAlfaBankPage() {
        $(By.xpath("//a[@href='https://alfabank.ru/']")).click();
        $(By.tagName("h2")).waitUntil(Condition.visible, 4000);
    }

    private void inputRequestInSearch() {
        $(By.xpath("//input[@title='Поиск']")).clear();
        $(By.xpath("//input[@title='Поиск']")).sendKeys("Альфа-Банк");
        $(By.xpath("//input[@name='btnK']")).click();
    }

    private String getDate() {
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy-HH-mm-ss");
        return dt.format(now);
    }

}
