package tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tests.appmanager.TestBase;
import tests.model.ProductData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;


public class MarketTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> products() {
        List<Object[]> list = new ArrayList<>();
        list.add(new Object[]{"мобильный телефон",1,new ProductData().withMark("Samsung").withPriceFrom(40000)});
        list.add(new Object[]{"наушники",1,new ProductData().withMark("Beats").withPriceFrom(17000).withPriceTo(25000)});
        return list.iterator();
    }


    @Test(dataProvider = "products")
    public void test(String name, int index, ProductData product) {
        if(name.equals("мобильный телефон")){
            app.goTo().mobileMenu();
        } else if (name.equals("наушники")){
            app.goTo().headphonesMenu();
        }
        app.searchPage().inputFilters(product);
        app.searchPage().assertFiltersTrue(product);
       String nameFromSearchPage = app.searchPage().getProductName(index);
       app.searchPage().choiceProduct(index);
        String nameFromProductPage = app.product().getProductName();
        assertEquals(nameFromSearchPage,nameFromProductPage);


    }


}
