package tests.appmanager;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;

public class TestBase {
    protected static final ApplicationManager app = new ApplicationManager();


    @BeforeSuite
    public void setUp() throws IOException {
        app.init();
        app.goTo().market();

    }

    @BeforeMethod
    public void login(){
        app.goTo().electronic();

    }

    @AfterMethod
    public void backToElectronic(){
        app.goTo().electronic();
    }

    @AfterSuite
    public void close(){
        app.close();
    }
}

