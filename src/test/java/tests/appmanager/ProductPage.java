package tests.appmanager;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ProductPage extends HelpConstructor {

    public String getProductName() {
       return $(By.tagName("h1")).getText();
    }
}
