package tests.model;

public class ProductData {

    private String mark;
    private int priceFrom;
    private int priceTo;

    public int getPriceTo() {
        return priceTo;
    }

    public ProductData withPriceTo(int priceTo) {
        this.priceTo = priceTo;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public ProductData withMark(String mark) {
        this.mark = mark;
        return this;
    }

    public int getPriceFrom() {
        return priceFrom;
    }

    public ProductData withPriceFrom(int priceFrom) {
        this.priceFrom = priceFrom;
        return this;
    }
}
